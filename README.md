
## Docker

#### START

_cd laradock_

_docker-compose up -d mysql apache2 redis_

#### RECREATE DOCKERS WITHOUT TOUCH VOLUMES

_docker-compose up --force-recreate -d mysql apache2 redis_

#### SSH

_docker exec -it [container] bash_

#### CONTAINERS LIST

_docker ps_

#### STOP

_docker stop $(docker ps -a -q)_

_docker rm $(docker ps -a -q)_

#### REBUILD IMAGE

_docker-compose build_

#### DOCKER IP ADDRESSES FOR ALL IMAGES

_docker inspect -f '{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq)_

#### VOLUME DATAS LIST

_docker volume ls_

#### PROCESS ACTIVE ON :80 PORT

_sudo lsof -i -n -P | grep ':80'_

#### DOCKER INFO

_docker inspect containername_

#### DOCKER LOG

_docker logs - f containername_